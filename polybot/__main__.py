"""
Main file.
"""
import logging

from polybot import target
from polybot.components import Camera, Robot1
from polybot.config import *
from polybot.lib import game, raspberry, supervisor

if IS_ROBOT_1:
    # Because Robot 2 can't compile the dependence opencv-contrib-python
    # There is not enough RAM
    from polybot.lib import aruco

logging.basicConfig(level=logging.DEBUG, format="(%(threadName)-9s) %(message)s")


def main():
    sup = supervisor.Supervisor()

    # (x,y) in mm and orientation in degrees
    r = Robot1(x=1000, y=1000, orientation=0)

    # Connection with the supervisor
    supervisor.ServerThread(sup, name="producer supervisor").start()
    supervisor.ProcessingThread(r, name="consumer supervisor").start()

    if IS_ROBOT_1:
        logging.debug("I am Robot 1")
        rasp_server = raspberry.Server()
        lid = lidar.Lidar()

        # Connection with the other raspberry
        raspberry.Rasp1ServerThread(rasp_server, name="producer raspberry 1").start()
        raspberry.Rasp1ProcessingThread(name="consumer raspberry 1").start()

        # Reading Aruco code with the camera
        cam = Camera()
        aruco.ArucoThread(cam, rasp_server, name="aruco raspberry 1").start()

        targets = target.robot1_square
        game.gameThread(r, lid, targets, name="Game thread 1").start()
    else:  # Robot 2
        logging.debug("I am Robot 2")
        rasp_client = raspberry.Client()

        # Connection with the other raspberry
        raspberry.Rasp2ClientThread(rasp_client, name="producer raspberry 2").start()
        raspberry.Rasp2ProcessingThread(name="consumer raspberry 2").start()


if __name__ == "__main__":
    main()
