"""
Targets that will be executed by robots.
"""
from polybot.components.stm32 import Instruction


class Target:
    """Target."""

    def __init__(self, x, y, instruction=None, priority=10, last=False):
        self.x = x
        self.y = y
        self.priority = priority
        self.skipped = False
        self.instruction = instruction
        self.last = last

    def __repr__(self):
        return f"Target({self.x}, {self.y}, instruction={self.instruction}, priority={self.priority}, last={self.last}, skipped={self.skipped})"


robot1_square = [
    Target(1000, 1500),
    Target(1500, 1500),  # instruction=(Instruction.ROTATION, 359)),
    Target(1500, 1000),
    Target(1000, 1000, last=True),
]

robot1_line = [
    Target(1500, 1000, priority=40),
    Target(2000, 1000, priority=30),
    Target(2500, 1000, priority=20),
    Target(3000, 1000, priority=10),
]
