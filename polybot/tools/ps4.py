import logging
import socket

from pyPS4Controller.controller import Controller

from polybot.config import *


class Client:
    """Client"""

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def write(self, data):
        """send data to server"""
        n = self.s.send(data)
        if n < len(data):
            logging.debug(f"sent {n} bytes instead of {len(data)}")

    def read(self, n):
        """read `n` bytes from server"""
        if self.s is None:
            return
        return self.s.recv(n)

    def connect(self):
        r = -1
        while r != 0:
            # return 0 when connection succeed
            r = self.s.connect_ex((RASP_SERVER, PORT))
        logging.debug("Connected to server")


MOVE_GO_FORWARD = b"Z"
MOVE_GO_BACKWARD = b"S"
MOVE_TURN_LEFT = b"Q"
MOVE_TURN_RIGHT = b"D"


class MyController(Controller):
    def __init__(self, c, **kwargs):
        Controller.__init__(self, **kwargs)
        self.c = c

    def on_x_press(self):
        print("bouton backward")
        self.c.write(MOVE_GO_BACKWARD)

    def on_triangle_press(self):
        print("bouton gauche")
        self.c.write(MOVE_TURN_LEFT)

    def on_circle_press(self):
        print("bouton droite")
        self.c.write(MOVE_TURN_RIGHT)

    def on_square_press(self):
        print("bouton forward")
        self.c.write(MOVE_GO_FORWARD)


def main():
    c = Client()
    c.connect()

    controller = MyController(
        c, interface="/dev/input/js0", connecting_using_ds4drv=False
    )
    # you can start listening before controller is paired, as long as you pair it within the timeout window
    controller.listen(timeout=60)


if __name__ == "__main__":
    main()
