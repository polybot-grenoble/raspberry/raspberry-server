"""
Config file
"""
import logging
import os


def set_conf(name, default):
    """Set config `name` with os.environ value if set or default to `default` value."""
    globals()[name] = default
    try:
        globals()[name] = type(default)(os.environ[name])
    except KeyError:
        # Environ variable isn't set, use default one
        pass
    except (TypeError, ValueError) as e:
        # Environ is set, but with a bad value
        logging.debug(f"Bad env '{name}' value or type")
        raise e


# Is code running on robot 1?
set_conf("IS_ROBOT_1", False)

# It should be the IP of the robot 1
# It's only used on the robot 2
set_conf("RASP_SERVER", "192.168.43.98")

# To display camera output on screen
set_conf("DEBUG", True)

#
#
# /!\ DON'T CHANGE SETTINGS BELOW FOR NO REASON /!\
#
#

# 0.0.0.0 is a special IP to make it reachable from localhost and from private network
# Host and port to communicate between both raspberry
set_conf("RASP_HOST", "0.0.0.0")
set_conf("RASP_PORT", 12321)

# Host and port to bind the server to
# Used for the supervisor
set_conf("HOST", "0.0.0.0")
set_conf("PORT", 51717)

# Number of messages from the supervisor/stm/raspberry that can be waiting from processing
set_conf("BUF_SIZE", 20)


# Override settings from `config_local.py` if available
try:
    from config_local import *
except ModuleNotFoundError:
    pass
