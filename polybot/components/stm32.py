"""
Class to communicate with the STM32.
"""
from enum import IntEnum, unique

from .uart import UART, Order


@unique
class Instruction(IntEnum):
    """Instruction values."""

    CONNECTION_REQUEST = 0x00
    LAUNCH_ROBOT = 0x01
    DISTANCE = 0x02
    ROTATION = 0x04
    MOVE_STATE = 0x06
    MOVE_VALUE = 0x07
    MOVE_STOP = 0x08
    OBSTACLE_TOF = 0x09
    MOVE_RESTART = 0x10
    ALLUMER_POMPE = 0x11
    ETEINDRE_POMPE = 0x12
    TRANSPORT = 0x13
    GALERIE_HAUT = 0x14
    GALERIE_BAS = 0x15
    SOL = 0x16
    COTE_INCLINE = 0x17
    COTE_HORIZONTAL = 0x18
    REPLIE = 0x19
    DEPLOIE = 0x1A


class Robot1(UART):
    """Robot 1 class."""

    def __init__(self, port="/dev/ttyAMA0", x=0, y=0, orientation=0):
        super().__init__(port)
        self.x = x
        self.y = y
        self.orientation = orientation
        self.connect_request()

    def connect_request(self):
        """Send the first query to make sure the connection works."""
        self.send_query(Order.COMMAND, Instruction.CONNECTION_REQUEST)

    def launch_robot(self):
        """Is launch slider removed.

        Returns:
            0 (robot wait)
            1 (robot launch)
        """
        return self.send_query(Order.COMMAND, Instruction.LAUNCH_ROBOT)

    def move_state(self):
        """Read movement state

        Returns:
            0 if movement is not finished
            1 if movement is finished
        """
        return self.send_query(Order.COMMAND, Instruction.MOVE_STATE)

    def obstacle_tof(self):
        """Ask if ToF sensors has detected an obstacle.

        Returns:
            A bitset of 8 bits for every sensors. 1 if detected, 0 otherwise.
        """
        return self.send_query(Order.COMMAND, Instruction.OBSTACLE_TOF)

    def move_restart(self):
        """Restart robot."""
        self.send_query(Order.COMMAND, Instruction.MOVE_RESTART)

    def move_forward(self, dist):
        """Move forward robot of `dist` [mm]."""
        if dist != 0:
            self.send_query(Order.COMMAND, Instruction.DISTANCE, dist)

    def move_backward(self, dist):
        """Move backward robot of `dist` [mm]."""
        if dist != 0:
            self.send_query(Order.COMMAND, Instruction.DISTANCE, -dist)

    def rotation(self, angle):
        """Rotate robot of `angle` [deg]."""
        if angle != 0:
            self.send_query(Order.COMMAND, Instruction.ROTATION, angle)

    def move_value(self, get_rotation):
        """Read distance or rotation achieved.

        Args:
            get_rotation (bool): Choose whether to return rotation or distance.

        Returns:
            Value in [mm] or [deg].
        """
        return self.send_query(Order.COMMAND, Instruction.MOVE_VALUE, get_rotation)

    def stop(self):
        """Stop the robot."""
        self.send_query(Order.COMMAND, Instruction.MOVE_STOP)

    def estimate_x_y(self):
        enc1 = self.read_encoder(1)
        enc2 = self.read_encoder(2)
        # make calculation to estimate x and y
        # modifiy x and y

    def estimate_angle(self):
        enc1 = self.read_encoder(1)
        enc2 = self.read_encoder(2)
        # make calculation to estimate angle
        # modify angle
