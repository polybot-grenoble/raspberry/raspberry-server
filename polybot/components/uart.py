"""
UART lib to communicate to STM32 using a predefined frame format.
"""
import logging
import struct
from enum import IntEnum, unique

import serial


@unique
class Order(IntEnum):
    """Order values."""

    COMMAND = 0x00
    READ = 0x01


class UART:
    """
    Base UART's device communication.
    An instance of :class:'UART' uses a serial port
    UART enables:
    - the connection by the UART, open and close the port
    - the send of query and the reception of response
    """

    def __init__(self, port):
        """
        Open serial device.

        Args:
            port: Serial device path. For instance '/dev/ttyUSB0' on linux.
        """
        self.open(port)

    def __del__(self):
        """Close the serial port."""
        self.close()

    def open(self, port):
        """
        Open the serial port.

        Args:
            port: Serial device path. For instance '/dev/ttyUSB0' on linux.
        """
        self.ser = serial.Serial(
            port, baudrate=115200, bytesize=8, parity="N", stopbits=1, timeout=500
        )

    def close(self):
        """Close the serial port."""
        try:
            self.ser.close()
        except AttributeError:
            pass

    def checksum(self, data):
        """
        Calculate the checksum of some data.

        Args:
            data: Input data bytes.

        Returns:
            Checksum byte value.
        """
        chk = 0
        for byte in data:
            chk ^= byte
        return (chk - 1) % 256

    def receive_response(self):
        """
        Receive a response. Verify the status and checksum.

        Returns:
            Received data, without header and checksum.

        Raises:
            ChecksumError: If the checksum isn't correct.
            StatusError: if status isn't Status.OK.
        """
        data_raw = self.ser.read(6)
        status, data, crc = struct.unpack(">BiB", data_raw)
        logging.debug(f"res: status({Status(status)}), data({data}), crc({crc})")

        if self.checksum(data_raw[:-1]) != crc:
            raise ChecksumError()

        if status != Status.OK:
            raise StatusError(status)

        return data

    def send_query(self, order, instruction, data=0):
        """
        Transmit an order to the stm32. This method automatically add
        the length and checksum bytes.

        Args:
            order: A value from Order class.
            instruction: A value from Instruction class.
            data: Data bytes.

        Returns:
            The received response.
        """
        frame = struct.pack(">BBi", order, instruction, data)
        frame += struct.pack(">B", self.checksum(frame))
        logging.debug(f"sending frame = {frame}")
        self.ser.write(frame)
        return self.receive_response()


@unique
class Status(IntEnum):
    """Response status from the Polybot device."""

    OK = 0x00
    TIMEOUT = 0x01
    UNKNOWN_COMMAND = 0x02
    QUERY_ERROR = 0x03
    CHECKSUM_ERROR = 0x04
    INTERNAL_ERROR = 0x05


class StatusError(Exception):
    """
    Thrown when an Polybot device did not respond with 'OK' status to the last
    command.
    """

    def __init__(self, status):
        """
        Args:
            status (int): Status code
        """
        super().__init__()
        self.status = Status(status)

    def __str__(self):
        return str(self.status)


class ChecksumError(Exception):
    """Thrown if a communication checksum error is detected."""

    pass


class ProtocolError(Exception):
    """Thrown if an unexpected response from the device is received."""

    pass
