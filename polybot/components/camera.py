"""
Class to communicate with a picamera.
"""
import time

from picamera import PiCamera
from picamera.array import PiRGBArray


class Camera:
    """Camera"""

    def __init__(self):
        self.camera = PiCamera()
        self.camera.resolution = (1280, 960)
        self.camera.framerate = 30
        self.camera.rotation = 0
        self.camera.iso = 1600
        time.sleep(2)  # Camera warm-up time

    def read(self):
        """Return a picture taken by the camera"""
        rawCapture = PiRGBArray(self.camera, size=self.camera.resolution)
        self.camera.capture(rawCapture, format="bgr", use_video_port=True)
        return rawCapture.array

    def stop(self):
        """Disable the camera"""
        self.camera.close()
