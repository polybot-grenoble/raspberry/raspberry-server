"""
The file contains all the functions required to use the Lidar on the robot.
"""
import cmath
import logging
import math
import statistics

import numpy as np
import ydlidar

# Length of the playing ground
M = 2000

# Width of the playing ground
N = 3000

# Sector size [deg]
SECTOR_SIZE = 10


class Point:
    """Utility class for Lidar."""

    def __init__(self, distance, angle, intensity):
        self.distance = distance
        self.angle = angle
        self.intensity = intensity

    @property
    def x(self):
        """Return x position in the cartesian plan."""
        c = cmath.rect(self.distance, self.angle)
        return round(c.real)

    @property
    def y(self):
        """Return y position in the cartesian plan."""
        c = cmath.rect(self.distance, self.angle)
        return round(c.imag)


class Lidar:
    """Lidar class."""

    def __init__(self):
        self.laser = self.config_lidarX2()
        self.scan = ydlidar.LaserScan()

        s = self.laser.initialize()
        if not s:
            raise OSError("Lidar might not be detected")

        s = self.laser.turnOn()
        if not s:
            raise OSError("Lidar might not be detected")

    def __del__(self):
        self.laser.turnOff()
        self.laser.disconnecting()

    @staticmethod
    def config_lidarX2():
        """
        Lidar configuration function.

        Some parameters are set according the model of lidar used.
        For a YDLIDAR X2

        Lidar Model Baudrate SampleRate(K) Range(m) Frequency (Hz) Intensity (bit)
        X2    6     115200   3             0.1~8    4-8(PWM)       false

        Single channel Voltage (V)
        true           4.8~5.2

        Otherelse see:
        https://github.com/YDLIDAR/YDLidar-SDK/blob/master/doc/Dataset.md

        Returns:
            The laser object containing configurations.
        """
        ports = ydlidar.lidarPortList()
        port = ""
        for key, value in ports.items():
            port = value
        laser = ydlidar.CYdLidar()

        # Set the UART com port
        laser.setlidaropt(ydlidar.LidarPropSerialPort, port)

        # Set the baudrate port
        laser.setlidaropt(ydlidar.LidarPropSerialBaudrate, 115200)

        # Distance analysis method used
        laser.setlidaropt(ydlidar.LidarPropLidarType, ydlidar.TYPE_TRIANGLE)

        #
        laser.setlidaropt(ydlidar.LidarPropDeviceType, ydlidar.YDLIDAR_TYPE_SERIAL)

        # Scan Frequency [Hz] (from 4 to 8)
        laser.setlidaropt(ydlidar.LidarPropScanFrequency, 8)

        # Sampling Rate [kHz] (can't be changed)
        laser.setlidaropt(ydlidar.LidarPropSampleRate, 3)

        # Single Channel (bool)
        laser.setlidaropt(ydlidar.LidarPropSingleChannel, True)

        # Maximal angle [deg]
        laser.setlidaropt(ydlidar.LidarPropMaxAngle, 180.0)

        # Minimal angle [deg]
        laser.setlidaropt(ydlidar.LidarPropMinAngle, -180.0)

        # Maximal distance detected [m] (up to 8.0)
        laser.setlidaropt(ydlidar.LidarPropMaxRange, 1.0)

        # Minimal distance detected [m] (minimal distance is 0.1m)
        laser.setlidaropt(ydlidar.LidarPropMinRange, 0.1)

        return laser

    def get_distances(self, rx, ry, r_angle, stop_dist):
        """
        Measure distances using lidar.

        Args:
            rx: X position of the robot [mm]
            ry: Y position of the robot [mm]
            r_angle: Angle of the robot [rad]
            stop_dist: Distance threshold [mm]

        Returns:
            An array containing the estimated distance for every SECTOR_SIZE degrees.
        """
        raw_points = self._get_points(r_angle)
        good_points = self._filter_points(raw_points, rx, ry, stop_dist)
        ranges = self._regroup_by_sector(good_points)
        ranges_mean = self._average_ranges(ranges)
        return ranges_mean

    def _get_points(self, r_angle):
        """
        Read points from lidar.

        Args:
            r_angle: Angle of the robot in [rad]

        Returns:
            An array of Point.
        """
        self.laser.doProcessSimple(self.scan)

        raw_points = []
        for point in self.scan.points:
            raw_points.append(
                Point(point.range * 1000, point.angle + r_angle, point.intensity)
            )

        return raw_points

    @staticmethod
    def _filter_points(raw_points, rx, ry, stop_dist):
        """
        Keep only the desired points.

        Args:
            raw_points: Array of Point
            rx: X position of the robot [mm]
            ry: Y position of the robot [mm]
            stop_dist: Distance threashold [mm]

        Returns:
            The useful points from raw_points.
        """
        good_points = []
        for point in raw_points:
            # Playground border
            if not (0 <= point.x + rx < N and 0 <= point.y + ry < M):
                continue

            # In desired range
            if not (0.01 < point.distance < stop_dist):
                continue

            # Keep only detected points
            if point.intensity > 500:
                good_points.append(point)

        return good_points

    @staticmethod
    def _regroup_by_sector(points):
        """
        Regroup points by sector of size SECTOR_SIZE [deg].

        Args:
            points: Array of Point to sort

        Returns:
            An array of array containing ranges.
        """
        ranges = [[] for _ in range(360 // SECTOR_SIZE)]

        for p in points:
            sector = (
                int(math.degrees(p.angle) / SECTOR_SIZE) % int(360 / SECTOR_SIZE)
            ) - 1
            ranges[sector].append(p.distance)

        return ranges

    @staticmethod
    def _average_ranges(ranges):
        """
        Compute average distances for each sector, remove noises.

        Args:
            ranges: Array to average

        Returns:
            Averages distance for each sectors.
        """
        ranges_average = []
        for r in ranges:
            if len(r) <= 1:
                ranges_average.append(0)
                continue

            mean = np.mean(r)
            std = statistics.stdev(r)

            lower = mean - std
            upper = mean + std
            without_noise = [x for x in r if lower < x < upper]

            if len(without_noise) == 0:
                ranges_average.append(0)
            else:
                ranges_average.append(statistics.mean(without_noise))

        return ranges_average
