import cmath
import logging
import math

import numpy as np

from polybot.components import Instruction, Order


class Action:
    """Action"""

    def __init__(self, instruction, value):
        self.instruction = instruction
        self.value = value

    def __repr__(self):
        return f"Action({self.instruction}, {self.value})"


def normalize_angle(angle):
    """Return `angle` between -180° and +180°."""
    if angle > 180:
        return angle - 360
    return angle


def angle(p1, p2):
    x = p2.x - p1.x
    y = p2.y - p1.y

    r = np.hypot(x, y)
    angle = np.rad2deg(np.arctan2(y, x))
    return r, angle


def handle(r, actions, current_target):
    if len(actions) == 0:
        return

    if not deplacement_fini(r, actions[0], current_target):
        return

    actions.remove(actions[0])

    # Start next action
    if len(actions) != 0:
        deplacement(r, actions[0])


def start_first_action(r, actions):
    deplacement(r, actions[0])


def actions_are_done(actions):
    return len(actions) == 0


def deplacement(r, action):
    r.send_query(Order.COMMAND, action.instruction, int(action.value))


def deplacement_fini(r, action, current_target):
    # x,y balise ? camera ?
    # value = r.move_value(action.instruction == Instruction.ROTATION)
    # logging.debug(f"value={value}")
    state = r.move_state()
    # Deplacement non fini
    if state == 0:
        return False

    # Deplacement fini
    if state == 0x0F:
        logging.debug("\nDeplacement fini")

        # update de x,y,angle avec la valeur théorique
        # TODO: le faire avec balise ou camera
        if action.action == Instruction.ROTATION:
            # mise à jour direction robot
            r.orientation = normalize_angle(r.orientation + action.value)
            logging.debug(f"Nouvelle orientation : {r.orientation}")
        else:
            # mise a jour position
            r.x = current_target.x
            r.y = current_target.y
            logging.debug(f"Nouvelle position : ({r.x}, {r.y})")

        return True
    raise Exception(f"Wrong state: {state}")
