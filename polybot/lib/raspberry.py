"""
Threads to communicate between 2 raspberry.
"""
import logging
import queue
import socket
import threading

from polybot.config import *

# Messages will be sent to the ProcessingThread with this queue
q1 = queue.Queue(BUF_SIZE)
q2 = queue.Queue(BUF_SIZE)


class Rasp1ServerThread(threading.Thread):
    """Thread that receive messages from the client raspberry."""

    def __init__(self, server, name=None):
        super().__init__(name=name)
        self.server = server

    def run(self):
        self.server.accept()

        while True:
            data = self.server.read(1)
            if not data:
                continue
            q1.put(data)
            logging.debug(f"Receive from client raspberry {data}")


class Rasp1ProcessingThread(threading.Thread):
    """Thread that process messages from the client raspberry."""

    def __init__(self, name=None):
        super().__init__(name=name)

    def run(self):
        while True:
            try:
                data = q1.get(timeout=1)
            except queue.Empty:
                continue
            self.process(data)

    def process(self, data):
        logging.debug(f"Process from client raspberry {data}")


class Server:
    """Server"""

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((RASP_HOST, RASP_PORT))
        self.s.listen()
        self.conn = None

    def write(self, data):
        """send data to supervisor"""
        if self.conn is None:
            return
        n = self.conn.send(data)
        if n < len(data):
            logging.debug(f"sent {n} bytes instead of {len(data)}")

    def read(self, n):
        """read `n` bytes from supervisor"""
        if self.conn is None:
            return
        return self.conn.recv(n)

    def accept(self):
        """accept the client connection"""
        self.conn, addr = self.s.accept()
        logging.debug(f"Connected by {addr[0]} from port {addr[1]}")


class Rasp2ClientThread(threading.Thread):
    """Thread that receive messages from the server raspberry."""

    def __init__(self, client, name=None):
        super().__init__(name=name)
        self.client = client

    def run(self):
        self.client.connect()

        while True:
            data = self.client.read(1)
            if not data:
                continue
            q1.put(data)
            logging.debug(f"Receive from server raspberry {data}")


class Rasp2ProcessingThread(threading.Thread):
    """Thread that process messages from the server raspberry."""

    def __init__(self, name=None):
        super().__init__(name=name)

    def run(self):
        while True:
            try:
                data = q1.get(timeout=1)
            except queue.Empty:
                continue
            self.process(data)

    def process(self, data):
        # Forward camera state to the stm
        self.stm.write(data)


class Client:
    """Client"""

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def write(self, data):
        """send data to server"""
        n = self.s.send(data)
        if n < len(data):
            logging.debug(f"sent {n} bytes instead of {len(data)}")

    def read(self, n):
        """read `n` bytes from server"""
        if self.s is None:
            return
        return self.s.recv(n)

    def connect(self):
        r = -1
        while r != 0:
            # return 0 when connection succeed
            r = self.s.connect_ex((RASP_SERVER, RASP_PORT))
        logging.debug("Connected to server")
