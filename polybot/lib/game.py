import logging
import threading
import time

from polybot.components import Instruction
from polybot.lib import movement


class gameThread(threading.Thread):
    """Thread that play a game by sending commands to the stm32."""

    GAME_DURATION = 100  # [seconds]

    def __init__(self, r, lidar, targets, name=None):
        super().__init__(name=name)
        self.r = r
        self.lidar = lidar
        self.targets = targets

    def game_finished(self, start_time):
        """Return True if every targets are done or game should stop."""
        elapsed_time = time.time() - start_time
        return len(self.targets) == 0 or elapsed_time > self.GAME_DURATION

    def pick_target(self):
        """Return a target with the highest priority."""
        if len(self.targets) == 0:
            raise ValueError("targets should not be empty")
        self.targets.sort(
            key=lambda e: (not e.last, not e.skipped, e.priority), reverse=True
        )
        return self.targets[0]

    def is_obstacle_blocking(self, current_target):  # fixme
        distance = 400  # [mm]
        d = self.lidar.get_distances(self.r.x, self.r.y, self.r.orientation, distance)
        logging.debug(f"d={d}")
        # todo: use current_target, rx,ry,roriention and d
        return False

    def target_to_actions(self, current_target):
        actions = []

        distance, rotation = movement.angle(self.r, current_target)
        rotation = movement.normalize_angle(rotation - self.r.orientation)

        if rotation != 0:
            actions.append(movement.Action(Instruction.ROTATION, rotation))
        if distance != 0:
            actions.append(movement.Action(Instruction.DISTANCE, distance))
        if current_target.instruction is not None:
            actions.append(movement.Action(*current_target.instruction))

        return actions

    def run(self):
        current_target = self.pick_target()
        actions = self.target_to_actions(current_target)

        # Wait for launch
        while not self.r.launch_robot():
            time.sleep(0.01)
        start_time = time.time()

        logging.debug("LANCEMENT DEPLACEMENTS")
        movement.start_first_action(self.r, actions)

        while not self.game_finished(start_time):
            logging.debug(f"actions={actions}")
            # if contre un mur or en l'air:
            # self.r.stop(), update(xy)

            if self.is_obstacle_blocking(current_target):
                logging.debug("obstacle detected")
                self.r.stop()
                time.sleep(0.5)
                current_target.skipped = True
                current_target = self.pick_target()
                actions = self.target_to_actions(current_target)
                movement.start_first_action(self.r, actions)

            elif movement.actions_are_done(actions):
                self.targets.remove(current_target)
                if len(self.targets) == 0:
                    continue

                current_target = self.pick_target()
                actions = self.target_to_actions(current_target)
                movement.start_first_action(self.r, actions)
            else:
                # Resend order if necessary
                movement.handle(self.r, actions, current_target)

        self.r.stop()
