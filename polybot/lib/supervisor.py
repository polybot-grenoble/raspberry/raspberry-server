"""
Threads to communicate with the supervisor.
"""
import logging
import queue
import socket
import subprocess
import threading

from polybot.config import *

# Messages will be sent to the ProcessingThread with this queue
q = queue.Queue(BUF_SIZE)


class ServerThread(threading.Thread):
    """Thread that receive messages from the supervisor."""

    def __init__(self, sup, name=None):
        super().__init__(name=name)
        self.sup = sup

    def run(self):
        self.sup.accept()

        while True:
            data = self.sup.read(1)
            if not data:
                continue
            q.put(data)
            logging.debug(f"Receive from supervisor {data}")


class ProcessingThread(threading.Thread):
    """Thread that process messages from the supervisor."""

    MOVE_GO_FORWARD = ord("Z")
    MOVE_GO_BACKWARD = ord("S")
    MOVE_TURN_LEFT = ord("Q")
    MOVE_TURN_RIGHT = ord("D")

    def __init__(self, r, name=None):
        super().__init__(name=name)
        self.r = r

        self.rasp_command = {
            100: "reboot",
            101: 'git log -1 --pretty=format:"%%h, %%ar: %%s"',
            102: "git pull",
        }

    def run(self):
        while True:
            try:
                data = q.get(timeout=1)
            except queue.Empty:
                continue
            self.process(data)

    def process(self, data):
        data = ord(data.decode())
        if data in self.rasp_command:
            logging.debug(f"Process from supervisor {data}, {self.rasp_command[data]}")
            self.exec_rasp_command(data)
        else:
            logging.debug(f"Forward to STM {data}")
            if data == self.MOVE_GO_FORWARD:
                self.r.move_forward(200)
            elif data == self.MOVE_TURN_LEFT:
                self.r.rotation(50)
            elif data == self.MOVE_TURN_RIGHT:
                self.r.rotation(-50)
            elif data == self.MOVE_GO_BACKWARD:
                self.r.move_backward(200)

    def exec_rasp_command(self, data):
        subprocess.run(self.rasp_command[data])


class Supervisor:
    """Supervisor"""

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((HOST, PORT))
        self.s.listen()
        self.conn = None

    def write(self, data):
        """send data to supervisor"""
        if self.conn is None:
            return
        n = self.conn.send(data)
        if n < len(data):
            logging.debug(f"sent {n} bytes instead of {len(data)}")

    def read(self, n):
        """read `n` bytes from supervisor"""
        if self.conn is None:
            return
        return self.conn.recv(n)

    def accept(self):
        """accept the supervisor connection"""
        self.conn, addr = self.s.accept()
        logging.debug(f"Connected by {addr[0]} from port {addr[1]}")
