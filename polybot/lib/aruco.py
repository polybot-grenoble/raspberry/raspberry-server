"""
Classes to manipulate the camera of the superviser

Credit:
  Tiziano Fiorenzani https://github.com/tizianofiorenzani/how_do_drones_work
  https://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/
"""
import logging
import threading
import time

import cv2
import imutils
import numpy as np
from cv2 import aruco

from polybot.components import Camera
from polybot.config import *

# The camera distortion arrays
camera_matrix = np.array(
    [[613.80715183, 0, 671.24584852], [0, 614.33915691, 494.57901986], [0, 0, 1]]
)  # *0.5
camera_distortion = np.array(
    [[-0.30736199, 0.09435416, -0.00032245, -0.00106545, -0.01286428]]
)

# Dictionary
aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_250)
parameters = aruco.DetectorParameters_create()


class Marker:
    """Marker"""

    RED = 0
    BLUE = 1
    GREEN = 2
    ROCK = 3
    RED = 4
    USER_PURPLE = 5
    USER_YELLOW = 6
    ROBOT_PURPLE = 7
    ROBOT_YELLOW = 8
    CENTER = 9
    NOT_KNOWN = 10

    def __init__(self, topLeft, topRight, bottomRight, bottomLeft, value):
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomRight = bottomRight
        self.bottomLeft = bottomLeft
        self.value = value

        self.x = (topLeft[0] + topRight[0] + bottomLeft[0] + bottomRight[0]) // 4
        self.y = (topLeft[1] + topRight[1] + bottomLeft[1] + bottomRight[1]) // 4

    def get_color(self):
        """Return the corresponding color of the marker."""
        if 1 <= self.value <= 5:
            return self.ROBOT_PURPLE
        if 6 <= self.value <= 10:
            return self.ROBOT_YELLOW
        if self.value == 13:
            return self.BLUE
        if self.value == 17:
            return self.ROCK
        if self.value == 36:
            return self.GREEN
        if self.value == 42:
            return self.CENTER
        if self.value == 47:
            return self.RED
        if 51 <= self.value <= 70:
            return self.USER_PURPLE
        if 71 <= self.value <= 90:
            return self.USER_YELLOW
        return self.NOT_KNOWN


class ArucoThread(threading.Thread):
    """Thread that try to read aruco code with the camera."""

    def __init__(self, cam, server, name=None):
        super().__init__()
        self.cam = cam
        self.server = server
        self.name = name

    def run(self):
        while True:
            markers = self.read_aruco()
            if markers is not None:
                logging.debug(f"Camera found {[m.value for m in markers]}")

    def stop(self):
        if DEBUG:
            cv2.destroyAllWindows()
        self.cam.stop()

    def read_aruco(self):
        frame = self.cam.read()
        frame = imutils.resize(frame, width=1000)

        # Find all the aruco markers in the image
        corners, ids, _ = aruco.detectMarkers(
            image=frame,
            dictionary=aruco_dict,
            parameters=parameters,
            cameraMatrix=camera_matrix,
            distCoeff=camera_distortion,
        )

        if ids is None:
            if DEBUG:
                cv2.imshow("Frame", frame)
                cv2.waitKey(1)
            return None

        ids = ids.flatten()
        markers = []

        for (markerCorner, value) in zip(corners, ids):
            c = markerCorner.reshape((4, 2))
            (topLeft, topRight, bottomRight, bottomLeft) = c

            topLeft = int(topLeft[0]), int(topLeft[1])
            topRight = int(topRight[0]), int(topRight[1])
            bottomRight = int(bottomRight[0]), int(bottomRight[1])
            bottomLeft = int(bottomLeft[0]), int(bottomLeft[1])
            markers.append(Marker(topLeft, topRight, bottomRight, bottomLeft, value))

        if DEBUG:
            aruco.drawDetectedMarkers(frame, corners)
            cv2.imshow("Frame", frame)
            cv2.waitKey(1)

        return markers
