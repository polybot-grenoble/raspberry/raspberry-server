# Raspberry Server

C'est un outil qui a pour but de :

- envoyer des informations de debug au superviseur
- envoyer les messages reçus depuis le STM32 au superviseur
- executer les commandes choisis par le superviseur
- communiquer avec le raspberry sur l'autre robot
- effectuer des tâches pendant l'épreuve tels que la détection du code ArUco

## Installation
# Testé pour raspberry pi 4 sous Raspberry pi OS 2022-04-04

```sh
sudo apt update
sudo apt upgrade
sudo apt install libcblas-dev
sudo apt install libhdf5-dev
sudo apt install libhdf5-serial-dev
sudo apt install libatlas-base-dev
sudo apt install libjasper-dev 
sudo apt install libqtgui4 
sudo apt install libqt4-test
pip install --upgrade pip
pip install -U numpy
pip install opencv-contrib-python==4.5.3.56
pip install imutils
```

## Utilisation

- Lancer le programme en faisant `python -m polybot`.

## Configuration

La configuration du raspberry qui a été faite se trouve [ici](https://polybot-grenoble.fr/wiki/index.php?title=Modules_techniques/Configuration_Raspberry).

La configuration du programme se trouve dans les fichiers `config.py` et `config_local.py`.


Le fichier `config.py` est commun aux raspberry et se trouve sur GitLab, il n'est pas à modifier.
Le fichier `config_local.py` est là pour override `config.py` pour des configurations précises, il ne se trouve pas sur GitLab. C'est ce fichier qu'il faut modifier si besoin.

Les variables d'environnements sont également disponible pour modifier la config.

Le code lit `config.py`, puis override `config_local.py` puis override avec les variables d'environnements.

## Code

Ce projet utilise python3.7+.
Le programme utilise des [threads](https://fr.wikipedia.org/wiki/Thread_(informatique)) pour faire plusieurs tâches en même temps.

### Architecture du projet

```sh
.
├── img # dossier contenant des images si necessaire
│    └── model.png 
├── LICENSE
├── polybot # dossier contenant le code
│    ├── components # dossier contenant des class reutilisables permettant de communiquer avec d'autres composants
│    │  ├── lidar.py
│    │  ├── stm32.py
│    │  └── uart.py
│    ├── config_local.py # fichier de configuration locale
│    ├── config.py # fichier de configuration globale du projet
│    ├── lib # dossier contenant la logique métier
│    │  ├── camera.py
│    │  ├── game.py
│    │  ├── movement.py
│    │  ├── raspberry.py
│    │  └── supervisor.py
│    ├── __main__.py # main du projet (code executé avec la commande `python -m polybot`)
│    ├── target.py # fichier de configuration de la trajectoire des robots
│    └── tools # dossier contenant des outils indépendants
│        └── ps4.py
└── README.md

```

### Modelisation du code du robot 1 (outdated)

Le robot 2 fonctionne de manière similaire.

![Modelisation du code](img/model.png)
